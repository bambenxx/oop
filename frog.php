<?php
require_once('animals.php');
class Frog extends Animal
{
    public $cold_blooded = "Yes";
    public function jump()
    {
        echo "hop hop";
    }
}
