<?php
require_once('animals.php');
class Ape extends Animal
{
    public $legs = 2;
    public $cold_blooded = "Yes";

    public function yell()
    {
        echo "Auooo";
    }
}
