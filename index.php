<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>

<body>
    <?php
    // require('animals.php');
    require('frog.php');
    require('ape.php');


    $sheep = new Animal("shaun");

    echo "Name : " . $sheep->name . "<br>";
    echo "Legs : " . $sheep->legs . "<br>";
    echo "Cold Blood : " . $sheep->cold_blooded . "<br><br>";

    $kodok = new Frog("Berot");

    echo "Name : " . $kodok->name . "<br>";
    echo "Legs : " . $kodok->legs . "<br>";
    echo "Cold Blood : " . $kodok->cold_blooded . "<br>";
    $kodok->jump();
    echo "<br><br>";


    $sunggokong = new Ape("Kera Sakti");

    echo "Name : " . $sunggokong->name . "<br>";
    echo "Legs : " . $sunggokong->legs . "<br>";
    echo "Cold Blood : " . $sunggokong->cold_blooded . "<br>";
    $sunggokong->yell();

    ?>
</body>

</html>